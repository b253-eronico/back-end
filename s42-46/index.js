const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

const app = express();
const port = process.env.PORT || 5000
const db = mongoose.connection

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");

mongoose.connect("mongodb+srv://admin:admin123@batch253-eronico.6rytlt2.mongodb.net/E-commerceAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

db.once("open", () => console.log("Now connected to MongoDB Atlas."));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoute);
app.use("/products", productRoute);


if(require.main === module ){
	app.listen(port, () => {
		console.log(`API is now online on port ${port}`)
	})
}

module.exports = app