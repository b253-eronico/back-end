const bcrypt = require("bcrypt");
const User = require("../models/User");
const auth = require("../auth");
const Products = require("../models/Product")


// =============== USER REGISTER ==================
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then(user => {

		if(user){
			return true
		} else {
			return false
		}
	}) .catch(err => err)
};

// ============= User Authentication ===============
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then(result => {
      if (result == null) {
        return { error: 'User not found' };
      } else {
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
        if (isPasswordCorrect) {
          return { access: auth.createAccessToken(result) };
        } else {
          return { error: 'Invalid password' };
        }
      }
    })
    .catch(err => {
      console.log(err);
      err.message = 'Error in loginUser';
      return { error: err.message };
    });
};


// ==================== Create Order ======================
// module.exports.order = async (data) => {
//   let isUserUpdated = false;
//   let isProductUpdated = false;

//   const user = await User.findById(data.userId);
//   console.log('User:', user);

//   await user.save()
//   .then(() => isUserUpdated = true)
//   .catch(err => console.error('user save error:', err));

//   if (!user) {
//     console.error(`User with ID ${data.userId} not found`);
//     return false;
//   }
//   user.orderedProducts.push({ productId: data.productId });
//   await user.save()
//     .then(() => isUserUpdated = true)
//     .catch(err => console.error(err));

//   const product = await Products.findById(data.productId);
//   console.log('Product:', product);
//   if (!product) {
//     console.error(`Product with ID ${data.productId} not found`);
//     return false;
//   }
//   product.userOrders.push({ userId: data.userId });
//   await product.save()
//     .then(() => isProductUpdated = true)
//     .catch(err => console.error(err));

//   console.log(isUserUpdated, isProductUpdated);

//   return isUserUpdated && isProductUpdated;
// };


module.exports.order = async (data) => {


    let isUserUpdated = await User.findById(data.userId).then(user => {

      user.orderedProducts.push(data.products);

      return user.save().then(user => {

        console.log(user);

        return true;

      }).catch(err => {

        console.error(err);

        return false;
      })
    });


let isProductUpdated = await Products.findById(data.productId)

  .then(product => {

    product.userOrders.push({ userId : data.userId });

    return product.save().then(product => {

      console.log(product);

      return true;

    }).catch(err => {

      console.error(err);

      return false;
    })
  })

    if(isUserUpdated && isProductUpdated){

        return `Successfully ordered`; 
   
    } else{

      return false;
    }
}

// ================= Retrieve User Details =============


module.exports.getUser = (data) => {

  return User.findById(data.userId)

    .select("-orderedProducts -password") // exluding orderedProducts and password
    .then(result => {

      return result;

    })

    .catch(err => err);
}
