const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");

// =========== Create Product Admin Only ===============
router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    

    productController.createProduct(req.body).then((resultFromController) => {
      res.send(resultFromController);
      })
      .catch((err) => {

        res.status(500).send({ error: err.message });
        
      });

  } else {

    res.send(false);

  }
});


// ================ GET ALL PRODUCTS ==================
router.get("/all", (req, res) =>{

	productController.getAllProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
})


// ======= GET ALL ACTIVE PRODUCTS
router.get("/active", (req, res) =>{

	productController.getAllActiveProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
})


module.exports = router;

// ================= GET SPECIFIC PRODUCTS ============
router.get("/:productId", (req, res) => {

  productController.getProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => err)
});


// ================= Update A product ===================
router.put("/:productId", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin)

  {
    productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => err)

  } else {

    return res.send(false)
  }
});

// ============= Archiving Product =====================
router.patch("/:productId/archive", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){

    productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

  } else {

    res.send(false);

  }
});

// ================ Activating a Product ====================
router.patch("/:productId/activate", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){

    productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

  } else {

    res.send(false);

  }
});