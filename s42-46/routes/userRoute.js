const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// =========== USER REGISTRATION =======
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});


// ========== User Authentication ==========
router.post('/login', (req, res) => {

  userController.loginUser(req.body).then(resultFromController => {

      if (resultFromController.error) { //codes to show errors if output shows blank array

        res.status(401).send(resultFromController);
      } else {

        res.send(resultFromController);
      }
    }).catch(err => {

      console.log(err); //codes to show errors if output shows blank array

      res.status(500).send({ error: 'Internal Server Error' });
    });
});

// ================== Create Order ====================
router.post("/order", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin === false) {
    const data = {

      userId: userData.id,

      productId: req.body.productId,
    };

    userController.order(data).then((resultFromController) => {

        console.log(resultFromController);

        res.send(resultFromController);
      })
      .catch((err) => {

        console.error(err); //codes to show errors if output shows blank array

        res.status(500).send("failed to process order");
      });
  } else {

    return res.send(false);
  }
});


// ================= Retrieve User Details =============
router.get("/:userId/profile", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  userController.getUser({ userId: userData.id }).then(resultFromController => res.send(resultFromController))

    .catch(err => res.send(err));

});




module.exports = router;