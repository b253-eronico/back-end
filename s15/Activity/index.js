console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

let firstName = "James Marvin";
console.log("First Name:" + " " + firstName);

let lastName = "Eronico";
console.log("Last Name:" + " " + lastName);

let years = "30";
console.log("Age:" + " " + years);

console.log("Hobbies:");

let hobbies = ["cars", "bigbikes", "online games"];
console.log(hobbies);

console.log("Work Address:");

let work = {
	houseNumber: "block1 lot13",
	street: "Pakigne",
	city: "Cebu",
	state: "MNGLA"
};
console.log(work);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is" + " " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false
	}
	console.log("My Full Profile: ")
	console.log(profile);

	let friend = "Bucky Barnes";
	console.log("My bestfriend is: " + friend);

	const lastLocation = "Arctic Ocean";
	secondLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);

//Do not modify
    //For exporting to test.js
    try{
        module.exports = {
            firstName, lastName, age, hobbies, workAddress,
            fullName, currentAge, friends, profile, fullName2, lastLocation
        }
    } catch(err){
 }