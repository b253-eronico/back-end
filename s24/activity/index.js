// console.log("WASAP!") ;

// Exponent Operator

// 4. Create a variable called getCube and use the exponent operator to compute for the cube 2.

const getCube = 2 ** 3;


// Template Literals

// 5. Using Template Literals, print out the value of the getCube variable with a message of The cube of 2 is

console.log(`The cube of 2 is ${getCube}.`);


// Array Destructuring

// 7. Destructure the given array from into the following variables:
// a. houseNumber
// b. street
// c. state
// d. zipCode

const address = ["258", "Washington Ave NW", "California", "90011"];
const [houseNumber, street, state, zipCode] = address;


// 8. then print out a message with the full address using Template literals

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}.`);

// Object Destructuring

// 9. Destructure the given object and print out a message with the details of the animal using Template literals.

const animal = {
  name: "Lolong",
  species: "saltwater crocodile",
  weight: "1075 kgs",
  measurement: "20 ft 3 in"
}
const { name, species, weight, measurement } = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);


// Arrow Functions

// 10. Loop through the given array using forEach, an arrow function and using the implicit return statement to print out the numbers.
let numbers = [1, 2, 3, 4, 5];
numbers.forEach(num => console.log(num));


// 11. Create a variable claed reduceNumber and using the reduce array methond and an arrow function, return the sum of all numbers in the given array

const reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(reduceNumber);

// Javascript Classes
// 12. Create a class of a dog and a constructor that will accept a name, age and breed as its properties.
class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

// 13. Create/instantiate a new object from the class Dog and console log the object.
const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}