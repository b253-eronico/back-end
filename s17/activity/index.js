// console.log("WASAP!");

/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:

function printUserInfo(){
	let user = "James Marvin Eronico";
	let userAge = 30;
	let userAddress1 = "Minglanilla\, Cebu City";
	let petName = "James";
	let petName2 = "Marvin";

	console.log("Hello, I\'m "+ " " + user + ".");
	console.log("I am "+ userAge + " " + "years old.");
	console.log("I live in"+ " " + userAddress1);
	console.log("I have a cat named"+ " " + petName + ".");
	console.log("I have a dog named"+ " " + petName2 + ".");
};



/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

function printFiveBands(){
	let band1 = "The Beatles";
	let band2 = "Taylor Swift";
	let band3 = "The Eagles";
	let band4 = "Rivermaya";
	let band5 = "Eraserheads";

	console.log(band1);	
	console.log(band2);	
	console.log(band3);	
	console.log(band4);	
	console.log(band5);	
};



/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

function printFiveMovies(){
	let movie1 = "Lion King";
	let movie2 = "Howl's Moving Castle";
	let movie3 = "Meet the Robinsons";
	let movie4 = "School of Rock";
	let movie5 = "Spirited Away";

	console.log(movie1);	
	console.log(movie2);	
	console.log(movie3);	
	console.log(movie4);	
	console.log(movie5);	
	
};



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};












//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printMyFiveBands,
		printMyFiveMovies,
		printFriends
	}
} catch(err){

}