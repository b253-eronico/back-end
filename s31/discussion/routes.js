// ctrl+c = stop the server


const http = require("http");

// creates a variable "port" to store the port number
const port = 4000;

// create a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response) => {

	// Accessing the "greeting" route returns a message of "Hello World"
	// "request" is an object that is sent via the client (browser)
	// The "url" property refers to the url or the link in the browser
	if (request.url == "/greeting") {
		response.writeHead(200,{ "Content-Type": "text/plain"});
		response.end("Hello again!");
		// accessing the "homepage" route returns a message of "this is the homepage"
	} else	if (request.url == "/homepage"){
		response.writeHead(200, { "Content-Type": "text/plain"});
		response.end("This is the homepage");
		// All other routes will return a message of "page not available"
	} else {
		// set a status code for the response a 404 means not found
		response.writeHead(404, { "Content-Type": "text/plain"});
		response.end("Page not available");
	}

});

// uses "server" and "port" variables created
server.listen(port);

// when server is running, consolve will print the message via terminal
console.log(`Server now accessible at localhost:${port}.`);

