// console.log("WASAP!");

// Create a simple roleplaying game.
// 1. Create a Character using an Object Constructor with required properties (name, classType, level).

function Character(name, classType, level) {
  this.name = name;
  this.classType = classType;
  this.level = level;
  this.hitPoints = 100;
  this.bag = [];
}




// let character = {
// 	name: "Xanxus",
// 	classType: "Tank",
// 	hp: 1000,
// 	level: 60,
// 	// 2. Add object methods for dodge, faint, attack.	
// 	attack: function() {
// 		console.log("Tank used shield bash")
// 	},
// 	dodge: function() {
// 		console.log( "Tank rolled and dodged then escaped")
// 	},
// 	faint: function() {
// 		console.log("This character loses all hp and fainted")
// 	},
// 	attack2: function() {
// 		console.log("Tank used God Strength and God's thrust")
// 	}
// }



// 	// 2. Add object methods for dodge, faint, attack.	
Character.prototype.dodge = function() {
  console.log(this.name + " dodged the attack!");
};

Character.prototype.faint = function() {
  this.hitPoints -= 10;
  console.log(this.name + " lost 10 hit points and now has " + this.hitPoints + " hit points left.");
};

Character.prototype.attack = function(monster) {
  console.log(this.name + " attacked the " + monster.type + "!");
  monster.takeDamage();
};



// 3.Add a property bag array to store all its items.
// this.bag []

// 4. Create a Monster using Object literals with the properties of type, level and object method of attack and takeDamage


const monster = {
  type: "Goblin",
  level: 3,
  hitPoints: 50,
  attack: function() {
    console.log("The " + this.type + " attacked you!");
  },
  takeDamage: function() {
    this.hitPoints -= 10;
    console.log("The " + this.type + " lost 10 hit points and now has " + this.hitPoints + " hit points left.");
  }
};

// let dragon = {
// 	name: "Flame Emperor",
// 	monsterType: "Fire Dragon",
// 	level: 50,
// 	// 2. Add object methods for dodge, faint, attack.	
// 	attack3: function() {
// 		console.log("Flame Emperor used Dragon swipe")
// 	},
	
// 	faint2: function() {
// 		console.log("t character loses all hp and fainted")
// 	}
// }


// function Monster(name, level) {
// 	this.name = name;
// 	this.level = level;
// 	this.health = level;
// 	this.attack = level;

// 	this.attack = function(target) {
// 		console.log(this.name + " " + "attacked and stunned" + " " + target.name)
// 		target.health -= this.attack;
// 		console.log(target.name + "'s health is now reduced to " + this.health)
// 		 if (target.health <= 0) {
// 			this.faint(target)
// 	};
// 		}

// 	this.faint = function(target) {
// 		console.log(target.name + "has fainted.");
// 	};
	
// 	this.miss = function(target) {
// 		console.log(target.name +" " + "did not lose hp");
// 	}
// };

// let fireDragon = new Monster ("Flame Emperor", 50);
// let xanxus = new Monster ("Xanxus", 60);

// character.attack();
// xanxus.attack(fireDragon);
// console.log(dragon);
// dragon.attack3();
// character.dodge();
// fireDragon.miss(xanxus);
// console.log(fireDragon);
// character.attack2(fireDragon);
// xanxus.attack(fireDragon);

// 5. Create a Merchant object with property array of items and object method of buy and sell.
// 	*if method buy is invoked, add the item to the bag array of Character
// 	*if method sell is invoke, delete the desired item from the array bag of Character


// Object Literal for Merchant
const merchant = {
  items: ["Health Potion", "Mana Potion", "Sword", "Shield"],
  buy: function(item, character) {
    character.bag.push(item);
    console.log(character.name + " bought a " + item + "!");
  },
  sell: function(item, character) {
    var index = character.bag.indexOf(item);
    if (index !== -1) {
      character.bag.splice(index);
      console.log(character.name + " sold a " + item + "!");
    }
  }
};


// 6. Create a new object of Character and invoke the methods to play the game.

let character = new Character("Xanxus", "Tank", 5);

// Playing the game
console.log(character.name + " is playing the game!");

monster.attack(); 
character.dodge(); 
character.attack(monster); 
monster.attack(); 
character.faint(); 

merchant.buy("Health Potion", character); 
merchant.sell("Health Potion", character); 



