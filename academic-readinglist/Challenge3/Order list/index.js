// console.log("WASAP!")

// Create a simple order system using object constructors and arrays with the following features/methods:
// 1. Add order
// 2. Show all orders
// 3. Remove product from order
// 4. Compute the total of order


// constructor for orders
function Order(customerName) {
  this.customerName = customerName;
  this.products = [];
}

//  constructor for products
function Product(name, price) {
  this.name = name;
  this.price = price;
}

// Methods of order
Order.prototype.addProduct = function(product) {
  this.products.push(product);
}

Order.prototype.removeProduct = function(productName) {
  const index = this.products.findIndex(product => product.name === productName);
  if (index !== -1) {
    this.products.splice(index, 1);
  }
}

Order.prototype.getTotal = function() {
  return this.products.reduce((total, product) => total + product.price, 0);
}

// Define an array to store orders
const orders = [];

// Add some sample data
const order1 = new Order("James");
order1.addProduct(new Product("Shirt", 20));
order1.addProduct(new Product("Pants", 30));
orders.push(order1);

const order2 = new Order("Marvin");
order2.addProduct(new Product("Shoes", 50));
orders.push(order2);


// 1. Add a new order
const order3 = new Order("Eronico");
order3.addProduct(new Product("Socks", 5));
orders.push(order3);

// 2. show all orders
console.log("All orders:");
console.log(orders);

// 3. Remove a product from an order
const productToRemove = "Shirt";
const orderToRemoveFrom = orders[0];
orderToRemoveFrom.removeProduct(productToRemove);
console.log(`Removed ${productToRemove} from ${orderToRemoveFrom.customerName}'s order`);
console.log(`New total: $${orderToRemoveFrom.getTotal()}`);

// 4. compute all orders
// console.log("All orders:");
orders.forEach(order => {
  console.log(`Order for ${order.customerName}:`);
  order.products.forEach(product => console.log(` - ${product.name}: $${product.price}`));
  console.log(`Total: $${order.getTotal()}\n`);
});


