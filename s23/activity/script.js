// console.log("Wasap!");




let trainer = {};



trainer.name = "Ash Ketchum"; // 
trainer.age = 6; // 
trainer.pokemon = ["Pikachu", "Charizard", "Blastoise", "Bulbasaur"]; 
trainer.friends = { // 
  hoenn: ["May", "Max"],
  kanto: ["Brock", "Misty"]
};




trainer.talk = function() {
  return "Pikachu! I choose you!";
};


console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);



console.log("Result of talk method");
console.log(trainer.talk);


function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;

  this.tackle = function(target){
    console.log(this.name + " tackled " + target.name)
    target.health -= this.attack; // subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
    console.log(target.name + "'s health is now reduced to " + target.health)
    if (target.health <= 0) {
      this.faint(target);
    }
  };
  
  this.faint = function(target) {
    console.log(target.name + " has fainted.");
  };
}

let geodude = new Pokemon("Geodude", 8);
let pikachu = new Pokemon("Pikachu", 12);
let mewtwo = new Pokemon("Mewtwo", 100);

geodude.tackle(pikachu);

console.log(pikachu);


mewtwo.tackle(geodude);

console.log(geodude);