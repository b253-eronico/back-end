
db.fruits.aggregate([
	{ 
		$match: {"onSale": true} 
	},
	{
		$count: "fruitsOnSale"
	}
]);

db.fruits.count({ "stock": { "$gt": 20 } })


db.fruits.aggregate([
  { "$match": { "onSale": true } },
  {
    "$group": {
      "_id": "$supplier",
      "avgPrice": { "$avg": "$price" }
    }
  }
])


db.fruits.aggregate([
  {
    "$group": {
      "_id": { "supplier": "$supplier", "name": "$name" },
      "maxPrice": { "$max": "$price" }
    }
  },
  {
    "$group": {
      "_id": "$_id.supplier",
      "maxPrices": { "$push": { "name": "$_id.name", "maxPrice": "$maxPrice" } }
    }
  }
])


db.fruits.aggregate([
  {
    "$group": {
      "_id": { "supplier": "$supplier", "name": "$name" },
      "minPrice": { "$min": "$price" }
    }
  },
  {
    "$group": {
      "_id": "$_id.supplier",
      "minPrices": { "$push": { "name": "$_id.name", "minPrice": "$minPrice" } }
    }
  }
])
