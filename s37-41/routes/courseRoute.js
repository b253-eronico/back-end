const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	// Ms Janie. Solution for activity s39 - did not edit courseController
	// const userData = auth.decode(req.headers.authorization);

	// if(userData.isAdmin) {
	// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	// } else {
	// 	res.send(false);
	// }


	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});


// Routes for retrieving all the courses
router.get("/all", auth.verify, (req,res) => { //need middleware auth

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.getAllCourses().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
	} else {

		return res.send(false)
	}

});

// Route for retrieving all the ACTIVE courses
		// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
router.get("/", (req,res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});


// Retrieving a specific course
		/*
			Steps:
			1. Retrieve the course that matches the course ID provided from the URL
		*/

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
	// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
	// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController)).catch(err => err)
});


// Route for updating a course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course


router.put("/:courseId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin)

	{
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => err)

	} else {

		return res.send(false)
	}
});


// failed s40 answer
// router.patch("/:courseId", auth.verify, (req, res) => {

//   const userData = auth.decode(req.headers.authorization);

//   if (!userData.isAdmin) 
//   {
//     return res.send(false);
//   }

//   courseController.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))

//     .catch(err => console.error(err));
// });


// Route to archiving a course
// A "PUT"/"PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases

//Session 40 - Activity Solution 2
router.patch("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});












module.exports = router;

