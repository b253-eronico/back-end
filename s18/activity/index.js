// console.log("WASAP!");


// Addition and Subtraction
function addNum(num1,num2){
	console.log(num1 + num2);
};
function subNum(num1,num2){
	console.log(num1 - num2);
};
console.log("Displayed sum of 5 and 15");
addNum(5,15);
console.log("Displayed difference of 20 and 5");
subNum(20,5);


// Multiplication and Division
function multiplyNum(num1,num2){
	return num1 * num2;
};
function divideNum(num1,num2){
	return num1 / num2;
};
let product = multiplyNum(50,10);
let quotient = divideNum(50,10);
console.log("The product of 50 and 10:")
console.log(product);
console.log("The quotient of 50 and 10:")
console.log(quotient);



// Get circle area
function getCircleArea(radius){
	return 3.1416*(radius**2)
};
let circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius:")
console.log(circleArea);




// Get average
function getAverage(num1,num2,num3,num4){

	return (num1 + num2 + num3 + num4) / 4;
}
let averageVar = getAverage(20,40,60,80);
console.log("The average of 20,40,60 and 80: ");
console.log(averageVar);
function checkIfPassed(score,total){
	let isPassed = (score / total) * 100 > 75;
	return isPassed;
	
}



// Check if scored passed
function checkIfPassed(score,total){
	let isPassed = (score / total) * 100 > 75;
	return isPassed;
	
}
let isPassingScore = checkIfPassed(38,50);
console.log("Is 38/50 a passing score?")
console.log(isPassingScore);


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}

